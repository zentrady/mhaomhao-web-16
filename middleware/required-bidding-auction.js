export default function ({ store, redirect }) {
  if (Object.keys(store.state.auction.biddingAuction).length === 0) {
    return redirect('/auction')
  }
}
